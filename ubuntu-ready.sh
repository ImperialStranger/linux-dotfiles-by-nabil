# Remove snap

sudo snap remove firefox
sudo snap remove snapd-desktop-integration
sudo snap remove gnome-3-38-2004 
sudo snap remove gtk-common-themes 
sudo snap remove bare 
sudo snap remove core20 
sudo snap remove snapd
sudo apt purge snapd

# Prevent snap from coming back

sudo cp nosnap.pref /etc/apt/preferences.d/nosnap.pref

# Switch to Mozilla PPA

sudo add-apt-repository ppa:mozillateam/ppa

echo '
Package: *
Pin: release o=LP-PPA-mozillateam
Pin-Priority: 1001
' | sudo tee /etc/apt/preferences.d/mozilla-firefox
echo 'Unattended-Upgrade::Allowed-Origins:: "LP-PPA-mozillateam:${distro_codename}";' | sudo tee /etc/apt/apt.conf.d/51unattended-upgrades-firefox

sudo apt update

# Firefox will be installed from Mozilla's official repository instead of snap from now
# Install Firefox through `sudo apt install firefox` command